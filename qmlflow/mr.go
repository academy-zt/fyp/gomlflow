package qmlflow

import (
	"net/http"
	"net/url"
)

func SendMRComment(token string, projectID string, mrID string, content string) error {
	postUrl := "https://jihulab.com/api/v4/projects/" + projectID + "/merge_requests/" + mrID + "/notes"
	resp, err := http.PostForm(postUrl,
		url.Values{"access_token": {token}, "body": {content}})
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}
