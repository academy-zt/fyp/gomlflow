package qmlflow

import (
	"io/ioutil"
	"net/http"

	"github.com/buger/jsonparser"
)

type RunInfo []byte

// Get the detailed information of the run from MLflow.
func QueryRunInfo(url string, runid string) (RunInfo, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	q := req.URL.Query()
	q.Add("run_id", runid)
	req.URL.RawQuery = q.Encode()
	var resp *http.Response
	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	result_body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return result_body, nil

}

// Get the name of the run
func (r RunInfo) Name() (string, error) {
	name, err := jsonparser.GetString(r, "run", "info", "run_name")
	if err != nil {
		return "", err
	}
	return name, nil
}

// Get the uri of artifacts
func (r RunInfo) ArtifactUri() (string, error) {
	uri, err := jsonparser.GetString(r, "run", "info", "artifact_uri")
	if err != nil {
		return "", err
	}
	return uri, nil
}

// Get the ID of the experiment the run belongs
func (r RunInfo) ExperimentID() (string, error) {
	id, err := jsonparser.GetString(r, "run", "info", "experiment_id")
	if err != nil {
		return "", err
	}
	return id, nil
}

// Get the Status of the run
func (r RunInfo) Status() (string, error) {
	status, err := jsonparser.GetString(r, "run", "info", "status")
	if err != nil {
		return "", err
	}
	return status, nil
}

// Get params of the run
func (r RunInfo) Params() (map[string]string, error) {
	params := make(map[string]string)
	_, err := jsonparser.ArrayEach(r, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		paramKey, err := jsonparser.GetString(value, "key")
		paramValue, err := jsonparser.GetString(value, "value")
		params[paramKey] = paramValue
	}, "run", "data", "params")
	if err != nil {
		return nil, err
	}
	return params, nil
}

// Get metrics of the run
func (r RunInfo) Metrics() (map[string]float64, error) {
	metrics := make(map[string]float64)
	_, err := jsonparser.ArrayEach(r, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		metricKey, err := jsonparser.GetString(value, "key")
		metricValue, err := jsonparser.GetFloat(value, "value")
		metrics[metricKey] = metricValue
	}, "run", "data", "metrics")
	if err != nil {
		return nil, err
	}
	return metrics, nil
}
