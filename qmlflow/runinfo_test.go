package qmlflow

import (
	"io/ioutil"
	"reflect"
	"strings"
	"testing"
)

func TestName(t *testing.T) {
	file, err := ioutil.ReadFile("mlflow.json")
	if err != nil {
		t.Error(err.Error())
	}
	result, err := RunInfo(file).Name()
	if err != nil {
		t.Error(err.Error())
	}
	if strings.Compare(result, "capricious-goat-148") != 0 {
		t.Error("The name does not match")
	}

}

func TestArtifactUri(t *testing.T) {
	file, err := ioutil.ReadFile("mlflow.json")
	if err != nil {
		t.Error(err.Error())
	}
	result, err := RunInfo(file).ArtifactUri()
	if err != nil {
		t.Error(err.Error())
	}
	if strings.Compare(result, "s3://mlflow/1/0125f51ff1244e1197151ce2d48be687/artifacts") != 0 {
		t.Error("The artifacts uri does not match")
	}

}

func TestExperimentID(t *testing.T) {
	file, err := ioutil.ReadFile("mlflow.json")
	if err != nil {
		t.Error(err.Error())
	}
	result, err := RunInfo(file).ExperimentID()
	if err != nil {
		t.Error(err.Error())
	}
	if strings.Compare(result, "1") != 0 {
		t.Error("The experiment id does not match")
	}

}

func TestStatus(t *testing.T) {
	file, err := ioutil.ReadFile("mlflow.json")
	if err != nil {
		t.Error(err.Error())
	}
	result, err := RunInfo(file).Status()
	if err != nil {
		t.Error(err.Error())
	}
	if strings.Compare(result, "FINISHED") != 0 {
		t.Error("The experiment id does not match")
	}

}

func TestParams(t *testing.T) {
	file, err := ioutil.ReadFile("mlflow.json")
	if err != nil {
		t.Error(err.Error())
	}
	result, err := RunInfo(file).Params()
	if err != nil {
		t.Error(err.Error())
	}
	actualParams := map[string]string{
		"alpha":    "0.5",
		"l1_ratio": "0.5",
	}
	if reflect.DeepEqual(result, actualParams) != true {
		t.Error("The params do not match")
	}

}

func TestMetrics(t *testing.T) {
	file, err := ioutil.ReadFile("mlflow.json")
	if err != nil {
		t.Error(err.Error())
	}
	result, err := RunInfo(file).Metrics()
	if err != nil {
		t.Error(err.Error())
	}
	actualMetrics := map[string]float64{
		"rmse": 0.7931640229276851,
		"r2":   0.10862644997792614,
		"mae":  0.6271946374319586,
	}
	if reflect.DeepEqual(result, actualMetrics) != true {
		t.Error("The metrics do not match")
	}

}
