package main

import (
	"bytes"
	"embed"
	"gomlflow/qmlflow"
	"log"
	"net/http"
	"os"
	"strings"
	"text/template"

	"github.com/gin-gonic/gin"
)

var accessToken string
var mlflowurl string // example: http://10.244.153.2:5002

//go:embed static/*
var templateDir embed.FS

type Comment struct {
	Eid     string
	Rid     string
	Rname   string
	Status  string
	MLUrl   string
	Uri     string
	Params  map[string]string
	Metrics map[string]float64
}

func main() {
	accessToken = os.Getenv("GITLABTOKEN")
	if strings.Compare(accessToken, "") == 0 {
		log.Fatal("Please specifiy GITLABTOKEN first")
	}
	mlflowurl = os.Getenv("MLFLOWURL")
	if strings.Compare(mlflowurl, "") == 0 {
		log.Fatal("Please specifiy MLFLOWENDPOINT first")
	}
	r := gin.Default()
	r.POST("/pgmlflow", parseRunID, parseRunInfo)
	r.Run(":9093")
}

func parseRunID(c *gin.Context) {
	runID, exist := c.GetPostForm("runid")
	if !exist {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "No runid provided",
		})
		c.Abort()
	}
	projectID, exist := c.GetPostForm("projectid")
	if !exist {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "No project id provided",
		})
		c.Abort()
	}
	mrID, exist := c.GetPostForm("mrid")
	if !exist {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "No mrid provided",
		})
		c.Abort()
	}
	c.Set("runid", runID)
	c.Set("projectid", projectID)
	c.Set("mrid", mrID)
	c.Next()

}

func parseRunInfo(c *gin.Context) {
	projectID := c.GetString("projectid")
	mrID := c.GetString("mrid")
	runid := c.GetString("runid")
	info, err := qmlflow.QueryRunInfo(mlflowurl+"/api/2.0/mlflow/runs/get", runid)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot get the information",
			"error": err.Error(),
		})
		c.Abort()
	}
	runName, err := info.Name()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot get the run name",
			"error": err.Error(),
		})
		c.Abort()
	}
	experimentID, err := info.ExperimentID()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot get the experiment ID",
			"error": err.Error(),
		})
		c.Abort()
	}

	uri, err := info.ArtifactUri()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot get the artifacts uri",
			"error": err.Error(),
		})
		c.Abort()
	}
	status, err := info.Status()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot get the run status",
			"error": err.Error(),
		})
		c.Abort()
	}
	params, err := info.Params()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot get the params",
			"error": err.Error(),
		})
		c.Abort()
	}
	metrics, err := info.Metrics()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot get the metrics",
			"error": err.Error(),
		})
		c.Abort()
	}
	commentToSend := Comment{
		Eid:     experimentID,
		Rid:     runid,
		Rname:   runName,
		Status:  status,
		MLUrl:   mlflowurl + "/#/experiments/" + experimentID + "/runs/" + runid,
		Uri:     uri,
		Params:  params,
		Metrics: metrics,
	}
	// import the template and execute it
	// tmpl, err := template.ParseFiles("static/comment.tmpl")
	tmpl, err := template.ParseFS(templateDir, "static/comment.tmpl")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot import the template",
			"error": err.Error(),
		})
		c.Abort()
	}
	buf := new(bytes.Buffer)
	err = tmpl.Execute(buf, commentToSend)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot execute the template",
			"error": err.Error(),
		})
		c.Abort()
	}
	err = qmlflow.SendMRComment(accessToken, projectID, mrID, buf.String())
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":   "Cannot send the comment to MR",
			"error": err.Error(),
		})
		c.Abort()
	}

}
